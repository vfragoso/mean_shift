// Copyright (C) 2013  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef CLUSTERING_NANOFLANN_EIGEN_ADAPTER_H_
#define CLUSTERING_NANOFLANN_EIGEN_ADAPTER_H_

// Implementation inspired from nanoflann-examples/vector_of_vectors_example.cpp

#include <nanoflann.hpp>
#include <Eigen/Dense>
#include <vector>

namespace clustering {

template <typename Scalar,
          int dim = -1,
          class Distance = nanoflann::metric_L2,
          typename IndexType = size_t>
class KDTreeAdapter {
 public:
  // Typedefs
  typedef
  KDTreeAdapter<Scalar> self_t;

  typedef typename
  Distance::template traits<Scalar, self_t>::distance_t metric_t;

  typedef
  nanoflann::KDTreeSingleIndexAdaptor<metric_t, self_t, dim, IndexType> index_t;

  // Constructor
  KDTreeAdapter(const std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >& v,
                const int _dim,
                const int leafMaxSize = 10) : data(v) {
    index = new index_t(_dim, *this,
                        nanoflann::KDTreeSingleIndexAdaptorParams(leafMaxSize,
                                                                  _dim));
    index->buildIndex();
  }

  // Destructor
  virtual ~KDTreeAdapter(void) {
    if (index) {
      delete index;
      index = 0;
    }
  }

  // Interface expected by KDTreeSingleIndexAdaptor
  const self_t& derived(void) const {
    return *this;
  }

  self_t& derived(void) {
    return *this;
  }

  // Must return the number of data points
  inline size_t kdtree_get_point_count(void) const {
    return data.size();
  }

  // Returns the distance between the vector "p1[0:size-1]" and the data point
  // with index "idx_p2" stored in the class:
  inline Scalar kdtree_distance(const Scalar *p1,
                                const size_t idx_p2,
                                size_t size) const {
    Scalar s = 0;
    for (size_t i = 0; i < size; i++) {
      const Scalar d = p1[i] - data[idx_p2][i];
      s += d*d;
    }

    return s;
  }

  // Returns the dim'th component of the idx'th point in the class:
  inline Scalar kdtree_get_pt(const size_t idx, int _dim) const {
    return data[idx][_dim];
  }

  // 
  template <class BBOX>
  bool kdtree_get_bbox(BBOX &bb) const {
    return false;
  }

  // Member
  index_t* index;

 protected:
  // Members
  const std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >& data;
};
}  // clustering
#endif  // CLUSTERING_NANOFLANN_EIGEN_ADAPTER_H_
