// Copyright (C) 2013  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVERCAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef MEAN_SHIFT_H_
#define MEAN_SHIFT_H_

#include "ThreadPool.h"
#include "nanoflann_eigen_adapter.h"
#include <nanoflann.hpp>
#include <glog/logging.h>
#include <Eigen/Dense>
#include <Eigen/LU>
#include <cmath>
#include <vector>
#include <future>
#include <utility>

namespace clustering {
template <typename Scalar = float>
class MeanShift {
 public:
  MeanShift(const int _dim,
            const Scalar _sigma = 1,
            const Scalar _lambda = 10,
            const int nworkers = 4,
            const Scalar _scalarCutOff = 4,
            const Scalar _eps = 1e-6)
      : sigmaInv(static_cast<Scalar>(1.0)/_sigma), lambda(_lambda),
        scalarCutOff(_scalarCutOff), eps(_eps), dim(_dim) {
    // Computing the determinant of the diagonal matrix
    Scalar det = pow(_sigma, dim);  // The determinant of the diagonal matrix
    Scalar piConstant = det*pow(static_cast<Scalar>(2.0*M_PI), dim);
    alpha = static_cast<Scalar>(1.0/sqrt(piConstant));

    // Threadpool
    tp = new ThreadPool(nworkers);
  }

  virtual ~MeanShift(void) {
    delete tp;
  }

  // cluster method
  // data: A list of vectors
  // clusterIds: The assigned cluster id for every point in data
  // modes: The peaks/modes of every cluster
  // returns: true if a succesful, false otherwise
  bool
  cluster(const std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >& data,
          std::vector<int>* clusterIds,
          std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >* modes);

  inline Scalar
  gaussianKernel(const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& x,
                 const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& mu) const;

 protected:
  /////////////////////////////////
  // Members
  // Gauss Positive definite matrix (the inverse is stored)
  const Scalar sigmaInv;  // Sigma of the kernel (bandwidth)
  const Scalar lambda;  // Threshold for merging modes
  const Scalar eps;  // Mean shift threshold
  const int dim;  // Dimension of the data
  Scalar alpha;  // Normalizing constant for Gaussian Kernel
  Scalar scalarCutOff;  // Threshold for ignoring data far from the current mode
  ThreadPool* tp;  // Thread Pool
  KDTreeAdapter<Scalar>* kdTree;  // KDTree (nanoflann)

  // Implementation of computeMode to be called asynchronously
  // Compute mode for data point
  bool
  computeMode(
    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& pt,
    const std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >& data,
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1>* mode) const;
};

////////////////////
// Implementation
template <typename Scalar>
bool MeanShift<Scalar>::computeMode(
    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& pt,
    const std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >& data,
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1>* mode) const {
  using Eigen::Matrix;
  using Eigen::Dynamic;

  *mode = pt;  // Initialize mode
  Scalar mean_shift;
  Scalar radius = scalarCutOff/sigmaInv;

  do {  // Gradient Ascent loop
    Matrix<Scalar, Dynamic, 1> prev_mu = *mode;  // Store mu
    mode->setZero();
    Scalar denominator = static_cast<Scalar>(0.0);  // Blank denominator

    // Compute meanshift using nano FLANN
    std::vector<std::pair<size_t, Scalar> > ret_matches;
    nanoflann::SearchParams params;

    const size_t nMatches = kdTree->index->radiusSearch(mode->data(),
                                                        radius,
                                                        ret_matches,
                                                        params);

    for (int k = 0; k < nMatches; k++) {
      const Matrix<Scalar, Dynamic, 1>& k_pt = data[ret_matches[k].first];
      Scalar commonFactor = gaussianKernel(k_pt, prev_mu);
      *mode += commonFactor*k_pt;
      denominator += commonFactor;
    }

    if (denominator == static_cast<Scalar>(0.0)) {
      *mode = prev_mu;
      return true;
    }

    *mode /= denominator;
    if (mode->hasNaN()) {
      LOG(INFO) << "Mode estimate contains NaN... Aborting";
      VLOG(1) << "Denominator: " << denominator;
      return false;
    }

    mean_shift = (*mode - prev_mu).norm();
  } while (mean_shift > eps);
  return true;
}

template <typename Scalar>
Scalar MeanShift<Scalar>::gaussianKernel(
    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& x,
    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>& mu) const {
  // Deviation from the mean
  Eigen::Matrix<Scalar, Eigen::Dynamic, 1> diff = x - mu;

  Scalar ndist = diff.squaredNorm()*sigmaInv;
  if (ndist >= scalarCutOff) return static_cast<Scalar>(0.0);
  Scalar dist = -static_cast<Scalar>(0.5)*ndist;

  Scalar exp_val = exp(dist);
  return alpha*exp_val;
}

template <typename Scalar>
bool MeanShift<Scalar>::cluster(
    const std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >& data,
    std::vector<int>* clusterId,
    std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1> >* modes) {
  using Eigen::Matrix;
  using Eigen::Dynamic;
  using std::pair;
  using std::vector;
  using std::future;
  using std::thread;

  // Check that we have enough data
  if (data.empty()) {
    LOG(INFO) << "No data was passed";
    return false;
  }

  // Check that we have good pointers
  if (!clusterId || !modes) {
    LOG(INFO) << "Invalid output pointers for <clusterId> and/or <modes>";
    return false;
  }
  
  // Clear the vectors
  const int npoints = data.size();
  vector<Matrix<Scalar, Eigen::Dynamic, 1> > computed_modes(npoints);
  bool success = false;
  clusterId->resize(npoints);
  modes->clear();

  // Build Index Using nanoflann...
  VLOG(1) << "Building KDTree ...";
  kdTree = new KDTreeAdapter<Scalar>(data, dim);

  // NOTE: If kernel and data dimensions mismatch, then at runtime an exception
  // from Eigen will be thrown.
  // For every point, calculate a mode by using a gradient ascent

  // This is using the Threadpool
  vector<future<bool> > results;
  Scalar mean_shift;
  for (int i = 0; i < npoints; i++) {
    // Schedule the job
    VLOG_EVERY_N(1, static_cast<int>(npoints*0.1))
        << "Scheduling jobs: " << i*100/(npoints-1) << " %";
    results.push_back(
        tp->enqueue(&MeanShift<Scalar>::computeMode, this,
                    std::cref(data[i]), std::cref(data), &computed_modes[i]));
  }  // For i-th data point

  // Get the results
  for (int i = 0; i < npoints; i++) {
    VLOG_EVERY_N(1, static_cast<int>(npoints*0.1))
        << "Progress: " << i*100/(npoints-1) << " %";
    success = results[i].get();
  }

  // Process found modes: Aggregate those that are very close to each other
  // by using the threshold.
  // Pick one point and use it as a mode, create one if the threshold says that
  // the point is not close enough.
  vector<int> modeFreq;
  int idx = npoints - 1;
  while (!computed_modes.empty()) {
    const Matrix<Scalar, Dynamic, 1>& comp_mode = computed_modes.back();

    const int nModes = modes->size();
    bool foundMode = false;
    for (int i = 0; i < nModes; i++) {
      Matrix<Scalar, Dynamic, 1>& mode = (*modes)[i];

      // L1 Norm criterion
      Scalar diff = (comp_mode - mode).cwiseAbs().sum();

      if (diff <= lambda) {  // Merge point into new mode
        mode *= modeFreq[i]++;  // Recover sum for average
        mode += comp_mode;  // Add new data point
        mode /= modeFreq[i];  // Renormalize
        foundMode = true;
        (*clusterId)[idx] = i;
        break;
      }
    }

    if (!foundMode) {  // Create a new mode
      modeFreq.push_back(1);  // First time seen this mode
      modes->push_back(comp_mode);  // Store mode
      (*clusterId)[idx] = modes->size() - 1;
    }
    idx--;
    computed_modes.pop_back();  // Delete comp_mode
  }

  delete kdTree;
  kdTree = 0;
  return true;
}
}  // clustering
#endif  // MEAN_SHIFT_H_
