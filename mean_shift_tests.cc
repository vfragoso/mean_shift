// Copyright (C) 2013  Victor Fragoso <vfragoso@cs.ucsb.edu>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of the University of California, Santa Barbara nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL VICTOR FRAGOSO BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "mean_shift.h"
#include <glog/logging.h>
#include "gtest/gtest.h"
#include <random>
#include <vector>

namespace clustering {
using clustering::MeanShift;
using Eigen::DiagonalMatrix;
using Eigen::Matrix;
using Eigen::Dynamic;
using Eigen::MatrixXd;
using Eigen::MatrixXf;
using std::random_device;
using std::normal_distribution;
using std::vector;

template <typename Scalar, int dims> inline
void genRandomPoint(normal_distribution<Scalar>& randn,
                    random_device& rng,
                    Matrix<Scalar, Dynamic, Dynamic>& pt) {
  for (int i = 0; i < dims; i++) {
    pt(i, 0) = randn(rng);
  }
}

TEST(MeanShift, Gaussian_Kernel_Eval) {
  // Has to be positive definite matrix, i.e., no zero entries,
  // and no negative values.
  double sigma = 1.0;
  const int dims = 3;
  MeanShift<double> ms(dims, sigma);

  Matrix<double, 3, 1> x;
  Matrix<double, 3, 1> mu;

  // Case 1
  x << 0, 0, 0;
  mu << 0, 0, 0;

  double eval = ms.gaussianKernel(x, mu);
  EXPECT_NEAR(eval, 6.349364e-02, 1e-3);  // Exact exp
  // EXPECT_NEAR(eval, 6.349364e-02, 0.01);  // Fast exp

  // Case 2
  x << 0, 0, 0;
  mu << 1, 1, 1;

  eval = ms.gaussianKernel(x, mu);
  // EXPECT_NEAR(eval, 1.416735e-02, 1e-3);  // Exact exp
  EXPECT_NEAR(eval, 1.416735e-02, 0.01);

  // Case 3
  x << 0.5, 0.5, 0.5;
  mu << 1, 1, 1;

  eval = ms.gaussianKernel(x, mu);
  EXPECT_NEAR(eval, 4.363850e-02, 1e-3);  // Exact exp
  // EXPECT_NEAR(eval, 4.363850e-02, 0.01);  // Fast exp
}

TEST(MeanShift, Clustering_NoData) {
  // Has to be positive definite matrix, i.e., no zero entries,
  // and no negative values.
  const int dims = 256;
  const int nworkers = 8;
  const int nclasses = 4;
  const int npoints = 1000; // points per class
  const float scalarCutOff = 4.0;
  const float sigma = 1.0;
  const float eps = 1e-3;
  float threshold = 10*dims;
  MeanShift<float> ms(dims, sigma, threshold, nworkers, scalarCutOff, eps);
  vector<Matrix<float, Dynamic, 1> > data;
  vector<int> clusterId;
  vector<Matrix<float, Dynamic, 1> > modes;
  ASSERT_FALSE(ms.cluster(data, &clusterId, &modes));
}

TEST(MeanShift, Clustering_FewDataPoints) {
  // Has to be positive definite matrix, i.e., no zero entries,
  // and no negative values.
  const int dims = 256;
  const int nworkers = 8;
  const int nclasses = 4;
  const int npoints = 1; // points per class
  const float scalarCutOff = 4.0;
  const float sigma = 1.0;
  const float eps = 1e-3;
  float threshold = 10*dims;
  MeanShift<float> ms(dims, sigma, threshold, nworkers, scalarCutOff, eps);

  // Generate two clusters using a normal distribution
  random_device rng;

  normal_distribution<float> randnA(-10.0, 2.0);
  normal_distribution<float> randnB(40.0, 3.0);
  normal_distribution<float> randnC(-40.0, 2.0);
  normal_distribution<float> randnD(60.0, 3.5);

  vector<Matrix<float, Dynamic, 1> > data(nclasses*npoints);
  int j = 0;
  MatrixXf aPt(dims, 1);
  MatrixXf bPt(dims, 1);
  MatrixXf cPt(dims, 1);
  MatrixXf dPt(dims, 1);

  for (int i = 0; i < npoints; i++) {
    genRandomPoint<float, dims>(randnA, rng, aPt);
    genRandomPoint<float, dims>(randnB, rng, bPt);
    genRandomPoint<float, dims>(randnC, rng, cPt);
    genRandomPoint<float, dims>(randnD, rng, dPt);
    data[j++] = aPt;
    data[j++] = bPt;
    data[j++] = cPt;
    data[j++] = dPt;
  }

  vector<int> clusterId;
  vector<Matrix<float, Dynamic, 1> > modes;
  ms.cluster(data, &clusterId, &modes);
  for (auto& mode : modes) {
    VLOG(1) << mode.transpose();
  }
  ASSERT_EQ(modes.size(), 4);
  ASSERT_EQ(clusterId.size(), data.size());
}

TEST(MeanShift, InvalidPointers) {
    // Has to be positive definite matrix, i.e., no zero entries,
  // and no negative values.
  const int dims = 256;
  const int nworkers = 8;
  const int nclasses = 4;
  const int npoints = 1; // points per class
  const float scalarCutOff = 4.0;
  const float sigma = 1.0;
  const float eps = 1e-3;
  float threshold = 10*dims;
  MeanShift<float> ms(dims, sigma, threshold, nworkers, scalarCutOff, eps);
  vector<Matrix<float, Dynamic, 1> > data;
  ASSERT_FALSE(ms.cluster(data, nullptr, nullptr)); 
}

TEST(MeanShift, Clustering_Gaussian_Data) {
  // Has to be positive definite matrix, i.e., no zero entries,
  // and no negative values.
  const int dims = 256;
  const int nworkers = 8;
  const int nclasses = 4;
  const int npoints = 1000; // points per class
  const float scalarCutOff = 4.0;
  const float sigma = 1.0;
  const float eps = 1e-3;
  float threshold = 10*dims;
  MeanShift<float> ms(dims, sigma, threshold, nworkers, scalarCutOff, eps);

  // Generate two clusters using a normal distribution
  random_device rng;

  normal_distribution<float> randnA(-10.0, 2.0);
  normal_distribution<float> randnB(40.0, 3.0);
  normal_distribution<float> randnC(-40.0, 2.0);
  normal_distribution<float> randnD(60.0, 3.5);

  vector<Matrix<float, Dynamic, 1> > data(nclasses*npoints);
  int j = 0;
  MatrixXf aPt(dims, 1);
  MatrixXf bPt(dims, 1);
  MatrixXf cPt(dims, 1);
  MatrixXf dPt(dims, 1);

  for (int i = 0; i < npoints; i++) {
    genRandomPoint<float, dims>(randnA, rng, aPt);
    genRandomPoint<float, dims>(randnB, rng, bPt);
    genRandomPoint<float, dims>(randnC, rng, cPt);
    genRandomPoint<float, dims>(randnD, rng, dPt);
    data[j++] = aPt;
    data[j++] = bPt;
    data[j++] = cPt;
    data[j++] = dPt;
  }

  vector<int> clusterId;
  vector<Matrix<float, Dynamic, 1> > modes;
  ms.cluster(data, &clusterId, &modes);
  for (auto& mode : modes) {
    VLOG(1) << mode.transpose();
  }
  ASSERT_EQ(modes.size(), 4);
  ASSERT_EQ(clusterId.size(), data.size());
}
}  // clustering
